package hellocucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class MyStepdefs {
    @Given("la grille contient un {string} en case {int}")
    public void laGrilleContientUnEnCase(String arg0, int arg1) {
        char arg = arg0.charAt(0);
        Game.BOARD[arg1] = arg;
        Game.drawTheField();
    }

    @When("player {string} plays")
    public void playerPlays(String arg0) {
        Game g1 = new Game("start user hard");
        Game.modeP2 = "hard";
        Game.mark_to_draw = 'O';
        Game.isFirst = false;
        g1.nextTurn();
        Game.drawTheField();
    }

    @Then("O prend place en case {int}")
    public void oPrendPlaceEnCase(int arg0) {
        Assert.assertEquals('O',Game.BOARD[arg0]);
    }

}
